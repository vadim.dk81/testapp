/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';


AppRegistry.registerComponent("TestRN_app", () => App);
