import React from 'react';
import { View, Text, StyleSheet, Button} from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';

//import { AuthContext } from "./context";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    button: {
        marginTop: 7,
        paddingTop: 10,
        borderRadius: 8
    }
});


const ScreenContainer = ({ children }) => {
  return  (
<View style={styles.container}>{children}</View>
);
}




export const Home = ({ navigation }) => (
    <ScreenContainer>
        <Text>Master List Screen</Text>
        <TouchableOpacity  style={{ backgroundColor: 'green', borderRadius: 15, margin: 5}} >
           <Button 
            title="React Native by Example"  
            onPress={() => navigation.push('Details', { name: 'React Native by Example'})}
           />
        </TouchableOpacity>
        <Button 
        title="React Native Shcool"  
        onPress={() => navigation.push('Details', { name: 'React Native Shcool'})}
        />
        <Button title="Drawer"  onPress={() => navigation.toggleDrawer()}/>
    </ScreenContainer>
)

export const Search = ({navigation}) => (
    <ScreenContainer>
        <Text>Screen Search</Text>
        <Button  title="Search 2" onPress={() => navigation.push('Search2')} />
        <Button  title="React Native Scool" onPress={() => 
            navigation.navigate('Home', {
                screen: 'Details',
                params: { name: "React Native School" }
            })} />
    </ScreenContainer>
)

export const Details = ({ route }) => (
    <ScreenContainer>
        <Text>Details Screen</Text>
{route.params.name && <Text>{route.params.name}</Text>}
      
    </ScreenContainer>
)

export const Search2 = () => (
    <ScreenContainer>
        <Text>Search2</Text>
      
    </ScreenContainer>
)

export const SignIn = ({ navigation }) => {
  return  ( 
    <ScreenContainer>
        <Text>SignIn Screen</Text>
        <Button style={styles.button} title="Sign In" onPress={() => alert('todo!')}/>
        <Button title="Create Account" onPress={() => navigation.push("CreateAccount")}/>
      
    </ScreenContainer>
)
}

export const CreateAccount = () => {
    return  ( 
      <ScreenContainer>
          <Text>Create Account Screen</Text>
          <Button title="Sign Up" onPress={() => alert('todo!')}/>
                 
      </ScreenContainer>
  )
  }
export const Profile = ({ navigation }) => {
    return  ( 
    <ScreenContainer>
        <Text>Profile Screen</Text>   
        <Button title="Drawer" onPress={() => navigation.toggleDrawer()}/>

    </ScreenContainer>
    )
}

  

